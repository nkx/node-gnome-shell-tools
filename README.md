#  [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]

> gnome-shell helper


## Install

```sh
$ npm install --save node-gnome-shell-tools
```


## Usage

```js
var nodeGshell = require('node-gnome-shell-tools');

nodeGshell('Rainbow');
```


## License

MIT © [nk](k46.net)


[npm-image]: https://badge.fury.io/js/node-gnome-shell-tools.svg
[npm-url]: https://npmjs.org/package/node-gnome-shell-tools
[travis-image]: https://travis-ci.org/nkx/node-gnome-shell-tools.svg?branch=master
[travis-url]: https://travis-ci.org/nkx/node-gnome-shell-tools
[daviddm-image]: https://david-dm.org/nkx/node-gnome-shell-tools.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/nkx/node-gnome-shell-tools
