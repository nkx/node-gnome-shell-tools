'use strict';
var assert = require('assert'),
    grunt = require('grunt'),
    _ = require('lodash'),
    GShell = require('../'),
    gsettings = GShell.Settings,
    orgEnabled = gsettings.get('enabled-extensions'),
    // path of test extension, when successfully installed
    extensionPath = null,
    // extension configuration for testing
    testCnf = {
      uuid: 'testing@node-gnome-shell-tools-test',
      extensionTarget: process.env.HOME+'/.local/share/gnome-shell/extensions',
      source: 'test/test-extension',
    };

// =======================================
// PRIVATE HELPER METHODS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 * clean up everithing
 */
function _cleanUp() {
  // reset list
  gsettings.set('enabled-extensions', _.without(orgEnabled, testCnf.uuid));
  // remove test extension 
  if(extensionPath && grunt.file.isDir(extensionPath)){
    grunt.file.delete(extensionPath);
  }
  // remove source of test extension 
  if(testCnf.source && grunt.file.isDir(testCnf.source)){
    grunt.file.delete(testCnf.source);
  }
}
/**
 * error callback handler. ensures cleaning when error is thrown.
 * @param {Object}   err  Error Object
 * @param {Function} done async function  
 */
function _rollback(err,done){
  _cleanUp();
  done(err);
}
/**
 * create the test extension 
 */
function _makeTestExtension(){
  // make temp file
  if(!grunt.file.isDir(testCnf.source)) {
    grunt.file.mkdir(testCnf.source);
  }
  // write dummy extension
  grunt.file.write(testCnf.source+'/extension.js', '// my extension');
  grunt.file.write(testCnf.source+'/metadata.json', JSON.stringify({
    uuid: testCnf.uuid,
    description: 'node-gnome-shell-tools test extension'
  }));
}

// =======================================
// TEST SETUP
// =======================================
describe('node-gnome-shell-tools extension setup', function () {
  var gextension = new GShell.Extension(testCnf),
      list = [];
  // dummy extension used for testing installation/removing
  _makeTestExtension();
  // test settings
  it('settings', function (done) {
    assert(gextension.validateSettings(), 'validation failed!');
    done();
  });
  // test extension enabling / disabling
  it('enable', function (done) {
    try {
      gextension.enableExtension();
      list = gsettings.get('enabled-extensions');
      assert(_.indexOf(list,testCnf.uuid) !== -1, 'extension not foun in list.');
      done();
    } catch(err) { _rollback(err,done); }
  });
  it('disable', function (done) {
    try {
      gextension.disableExtension();
      list = gsettings.get('enabled-extensions');
      assert(_.indexOf(list,testCnf.uuid) === -1, 'extension still found in list');
      done();
    } catch(err) { _rollback(err,done); }
  });
});
// =======================================
// TEST INSTALLATION
// =======================================
describe('node-gnome-shell-tools extension installation', function () {
  var gextension = new GShell.Extension(testCnf);
  it('install', function (done) {
    try {
      extensionPath = gextension.getExtensionPath(); // store the path for the cleanUp method
      gextension.installExtension();
      assert(grunt.file.isDir(extensionPath), 'enabledExtension-test went wrong');
      done();
    } catch(err) { _rollback(err,done); }
  });
  it('remove', function (done) {
    try {
      gextension.removeExtension();
      assert(!grunt.file.isDir(gextension.getExtensionPath()), 'extension still exists');
      done();
    } catch(err) { _rollback(err,done); }
  });
});
// =======================================
// CLEANING
// =======================================
// ensure that all gets cleaned on the end
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
describe('node-gnome-shell-tools cleaning', function () {
  it('cleaning', function () {
    _cleanUp();
  });
});
