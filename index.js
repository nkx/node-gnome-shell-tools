/*jshint camelcase: false, esnext: true, evil: true, strict: false*/
/**
 * [[Description]]
 */
var shelljs = require('shelljs'),
    GSettings = require('node-gsettings'),
    gsettings = new GSettings('org.gnome.shell'),
    fs = require('fs'),
    _ = require('lodash');

function GShellExtension(config) {
  this.gsettings = gsettings;
  this.cnf = _.extend({
    extensionTarget: process.env.HOME+'/.local/share/gnome-shell/extensions',
    devMode: false,
  },config);
}
GShellExtension.prototype = {
  validateSettings: function () {
    var cnf = this.cnf;
    if(!cnf || !cnf.uuid) {
      // config not valid
      return false;
    } else if(!cnf.extensionTarget || !fs.existsSync(cnf.extensionTarget)) {
      // extension target not found
      return false;
    }
    return true;
  },
  /**
   * enable extension (gsettings)
   * @param {String} [uuid=this.cnf.uuid] extension uuid
   */
  enableExtension: function(uuid) {
    uuid = uuid || this.cnf.uuid;
    var enabled = this.gsettings.get('enabled-extensions');
    if(enabled){
      enabled = _.unique(enabled.concat(uuid));
      this.gsettings.set('enabled-extensions',enabled);
    }
  },
  /**
   * disable extension (gsettings)
   * @param {String} [uuid=this.cnf.uuid] extension uuid
   */
  disableExtension: function(uuid) {
    uuid = uuid || this.cnf.uuid;
    var enabled = this.gsettings.get('enabled-extensions');
    if(enabled){
      enabled = _.without(enabled,uuid);
      this.gsettings.set('enabled-extensions',enabled);
    }
  },
  /**
   * install extension
   * @param {Boolean} devMode when true link src instead of copying files
   */
  installExtension: function(uuid,source) {
    uuid = uuid || this.cnf.uuid;
    source = source || this.cnf.source;
    var targetDir = this.getExtensionTarget() + uuid;
    this.removeExtension(uuid);
    if(!shelljs.test('-d', targetDir)) {
      shelljs.mkdir(targetDir);
    }
    if(source && typeof(source)==='string'){
      shelljs.cp('-f',source+'/*',targetDir);
    } else if(source instanceof Array) {
      source.forEach(function (file) {
        shelljs.cp('-f', file, targetDir);
      });
    }
    this.enableExtension(uuid);
  },
  /**
   * install extension
   * @param {Boolean} devMode when true link src instead of copying files
   */
  linkExtension: function(uuid,source) {
    uuid = uuid || this.cnf.uuid;
    source = source || this.cnf.uuid;
    if(!fs.existsSync(source)) {
      return false;
    }
    this.removeExtension(uuid);
    shelljs.ln('-s', source, this.getExtensionTarget() + uuid);
    this.enableExtension(uuid);
  },
  /**
   * remove extension
   * @param {String} [uuid=this.cnf.uuid] extension uuid
   */
  removeExtension: function(uuid) {
    uuid = uuid || this.cnf.uuid;
    var targetDir = this.getExtensionTarget() + uuid;
    if(shelljs.test('-L',targetDir)) {
      // remove symbolic link
      console.log("Remove Link!");
      //shelljs.delete(targetDir);
    } else if(shelljs.test('-d',targetDir)) {
      // remove directory
      //console.log('Remove Extension');
      shelljs.rm('-r', targetDir);
    }
    this.disableExtension(uuid);
  },
  /**
   * installation path
   * @returns {string} path to installed extension
   */
  getExtensionPath: function(uuid){
    uuid = uuid || this.cnf.uuid;
    return this.getExtensionTarget() + uuid;
  },
  /**
   * installation path
   * @returns {string} path to directory containing extensions
   */
  getExtensionTarget: function(){
    var dir = this.cnf.extensionTarget;
    return dir+((!/\/$/.test(dir) && '/') || '');
  },
  /**
   * reload gnome-shelll
   */
  reload: function(){
    shelljs.exec('gnome-shell --replace &', function () {
      console.log('Reay');
    });
  }
};

module.exports = {
  Settings: gsettings,
  Extension: GShellExtension
};
